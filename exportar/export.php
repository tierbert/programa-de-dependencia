<?php 
include_once '../functions.php';

$result = getDataToExport($CONNECTION);


if (!$result) {
    exit(mysqli_error($CONNECTION));
}
$alunos_disc = array();
if (mysqli_num_rows($result) > 0) {
    while ($row = mysqli_fetch_assoc($result)) {
        $alunos_disc[] = $row;
    }
}
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=Alunos por Disciplina.csv');
$output = fopen('php://output', 'w');
fputcsv($output, array('Matrícula', 'Aluno', 'Disciplina', 'Curso', 'Repetições'));
 
if (count($alunos_disc) > 0) {
    foreach ($alunos_disc as $row) {
        fputcsv($output, $row);
    }
    $sucesso = true;
    
} else {
	$sucesso = false;
}

?>
