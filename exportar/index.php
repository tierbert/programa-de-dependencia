
	<?php 
		include_once '../functions.php'; 
		setLoggedArea(true, $SITE_URL);
		$title = 'Programa de Dependência';
		getHeader();
	?>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6 pl-5 text-center">
				<h3 id='page-intro' class="text-muted">Exportar dados</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8 pl-5">
				<p class="text-muted lead text-center" id='lead'> Clique no botão abaixo para baixar os dados do sistema em formato CSV. </p>
				<form id='updateform' method="post" action="export.php">
				<div id="btn-holder">
					<button type="submit" id='submitBtn' class="btn btn-primary btn-lg btn-block">Download</button>


				</div>


			</div>
		</div>
		
<?php function scripts(){ ?>

	<script type="text/javascript">
			
		const btnholder = document.getElementById('btn-holder');
		const btn = document.getElementById('submitBtn');
		const updateform = document.getElementById('updateform');
		const introText = document.getElementById('page-intro');
		const leadText = document.getElementById('lead');
		

		btn.addEventListener('click', e => {
			btnholder.innerHTML = '<center><div class="spinner-grow" role="status"><span class="sr-only">Loading...</span></div></center>'
			lead.innerHTML = '';
			updateform.submit();

			setTimeout(() => {
				lead.innerHTML = 'Dados exportados...';
				btnholder.innerHTML = '<a href="/programa-de-dependencia" class="btn btn-primary btn-lg btn-block"> Voltar a página inicial </a>'
			}, 3000)
		})
		

	</script>
<?php } ?>
<?php getFooter(); ?>


