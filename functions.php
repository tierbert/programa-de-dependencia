<?php 
include_once 'connection.php';

// Setting ENV VARS
$SITE_URL = "http://" . $_SERVER['SERVER_NAME'].'/programa-de-dependencia/';
$INTIAL_SEMESTER_TO_DOWNLOAD = 2015;
// end of ENV VARS

function testDBconnection($CONNECTION){
	if ($result = mysqli_query($CONNECTION, "SELECT * FROM teste")) {
	    printf("Select returned %d rows.\n", mysqli_num_rows($result));

	    /* free result set */
	    mysqli_free_result($result);
	}
}
function testDBInsertion($CONNECTION){
	if ($result = mysqli_query($CONNECTION, "SELECT * FROM alunos_disc")) {
	    printf("Select returned %d rows.\n", mysqli_num_rows($result));

	    /* free result set */
	    mysqli_free_result($result);
	}
}
function downloadFirstPage($CONNECTION){
		$postdata = (object)[
			'token' => '5e0d3b28bd55aa5bbb1d35ff89a08b93',
			'format' => 'json',
			'idOrg' => 0,
			'statusMatriculaDisciplina' =>  'REPROVADO'
		];

	//echo CallAPI('https://guanambi.jacad.com.br:443/academico/api/v1/academico/aluno/matriculas/disciplinas/', $postdata);
	$alunos_disc = json_decode(CallAPI('https://guanambi.jacad.com.br:443/academico/api/v1/academico/aluno/matriculas/disciplinas/', $postdata));

	foreach ($alunos_disc as $aluno) {
		var_dump(saveInDB($CONNECTION, $aluno));
	}
	testDBInsertion($CONNECTION);
}
function wipeAllData($CONNECTION){
	$sql = 'truncate alunos_disc;';
	return (mysqli_query($CONNECTION, $sql));
}
function getValidSemesters($INTIAL_SEMESTER_TO_DOWNLOAD){
	$postdata = (object)[
			'token' => '5e0d3b28bd55aa5bbb1d35ff89a08b93',
			'format' => 'json',
			'idOrg' => 0
	];
	$semesters = json_decode(CallAPI('https://guanambi.jacad.com.br:443/academico/api/v1/academico/periodos-letivos/', $postdata));
	$valid_semesters = array();
	foreach ($semesters as $semester) {
		if($semester->ano >= $INTIAL_SEMESTER_TO_DOWNLOAD){
			array_push($valid_semesters, $semester->idPeriodoLetivo);
		}
	}
	return $valid_semesters;
}
function getAllData($CONNECTION, $INTIAL_SEMESTER_TO_DOWNLOAD){
	
	wipeAllData($CONNECTION);

	$semesters = getValidSemesters($INTIAL_SEMESTER_TO_DOWNLOAD);
	foreach ($semesters as $semester) {
		$page = 1;
		$isDownloading = true;
		while($isDownloading){
			$isDownloading = downloadPage($CONNECTION, $page, $semester);
			$page++;
		}
	}

	return true;
}
function downloadPage($CONNECTION, $page, $semester){
	$offset = ($page-1)*300;
	$postdata = (object)[
		'token' => '5e0d3b28bd55aa5bbb1d35ff89a08b93',
		'format' => 'json',
		'idOrg' => 0,
		'offset' => $offset,
		'idPeriodoLetivo' => $semester,
		'statusMatriculaDisciplina' =>  'REPROVADO'
	];

	$alunos_disc = json_decode(CallAPI('https://guanambi.jacad.com.br:443/academico/api/v1/academico/aluno/matriculas/disciplinas/', $postdata));
	
	if(sizeof($alunos_disc) > 0){
		foreach ($alunos_disc as $aluno) {
			saveInDB($CONNECTION, $aluno);
		}
		// testDBInsertion($CONNECTION);
		return true;
	} else {
		return $alunos_disc;	
	}
	
}

function getDataToExport($CONNECTION){
	$sql = "SELECT ra, nome, disciplina, curso, count(disciplina) FROM alunos_disc GROUP BY nome, disciplina, ra, curso ORDER BY count(disciplina) DESC";

	$result = mysqli_query($CONNECTION, $sql);

	return $result;
}

function saveInDB($CONNECTION, $data){
	$sql = '
		INSERT INTO alunos_disc (
		idAluno, idMatricula, idMatriculaDisciplina, idPeriodoLetivo, idTurma, quantFaltas, idDisciplinaProfessor, ra, email, login, senha, cpf, dataInicioPeriodoLetivo, dataTerminoPeriodoLetivo, nome, periodoLetivo, matrizCurricular, curso, turma, statusMatricula, idDisciplina, disciplina, statusMatriculaDisciplina, mediaFinal, _nomeRed) 
		values (
		'.$data->idAluno.', 
		'.$data->idMatricula.', 
		'.$data->idMatriculaDisciplina.', 
		'.$data->idPeriodoLetivo.', 
		'.$data->idTurma.', 
		'.$data->quantFaltas.', 
		'.$data->idDisciplinaProfessor.', 
		"'.$data->ra.'",
		"'.$data->email.'",
		"'.$data->login.'",
		"'.$data->senha.'",
		"'.$data->cpf.'",
		"'.$data->dataInicioPeriodoLetivo.'",
		"'.$data->dataTerminoPeriodoLetivo.'",
		"'.$data->nome.'",
		"'.$data->periodoLetivo.'",
		"'.$data->matrizCurricular.'",
		"'.$data->curso.'",
		"'.$data->turma.'",
		"'.$data->statusMatricula.'",
		"'.$data->idDisciplina.'",
		"'.$data->disciplina.'",
		"'.$data->statusMatriculaDisciplina.'",
		"'.$data->mediaFinal.'",
		"'.$data->_nomeRed.'"
		)
	';
	return (mysqli_query($CONNECTION, $sql));
	//return $sql;

}
function CallAPI($URI, $data = false){
			
    $ch = curl_init($URI);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_URL, $URI);

    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
}
function getDataFromDB($CONNECTION){
	$sql = '
		SELECT *, count(disciplina)
		FROM alunos_disc
		GROUP BY nome, disciplina
		ORDER BY count(disciplina) DESC
		LIMIT 3000
	';

	$alunos = mysqli_query($CONNECTION, $sql);
	return $alunos;
}

function processDataFromDB($CONNECTION, $aluno){
	$sql = 'SELECT *, count(disciplina) FROM alunos_disc where nome = "'.$aluno.'"';
	$result = mysqli_query($CONNECTION, $sql);
    var_dump($result);
	return $result;
}
function getPaginatedDataFromDB($CONECTION, $start, $length){
	$sql = "SELECT *, count(disciplina)
		FROM alunos_disc
		GROUP BY nome, disciplina
		ORDER BY count(disciplina) DESC
		LIMIT ".$start." ,".$length." ";

	$alunos = mysqli_query($CONNECTION, $sql);
	return $alunos;
}
function getAllowedUsers(){
	$allowedUsersIds = [1472, 1020, 1425, 1473, 552, 1372, 264];

	$usersData = array();

	foreach ($allowedUsersIds as $allowedUserId) {
		$postdata = (object)[
			'token' => '5e0d3b28bd55aa5bbb1d35ff89a08b93',
			'format' => 'json',
			'idFuncionario' =>  $allowedUserId
		];

		$result = json_decode(CallAPI('https://guanambi.jacad.com.br:443/academico/api/v1/rh/funcionarios/', $postdata));
		array_push($usersData, $result[0]);
	}

	
	return $usersData;
	
}
function authUser($login, $pass){
	$users = getAllowedUsers();
	$auth = false;
	foreach ($users as $user) {
		if($user->_login == $login && $user->_senha == $pass){
			$auth = true;
			if(!isset($_SESSION)) { 
		        session_start(); 
		    } 
			$_SESSION['auth'] = $auth;
			$_SESSION['user_id'] = $user->idFuncionario;
		}
	}
	return $auth;
}
function isUserLogged(){
	if(!isset($_SESSION)) { 
        session_start(); 
    } 
	if(isset($_SESSION['auth'])){
		return true;
	} else {
		return false;
	}
}
function setLoggedArea($isLoginMandatory, $SITE_URL){
	if($isLoginMandatory){
		if (!isUserLogged()){
			header("Location: ".$SITE_URL);
		}
	} else {
		if (isUserLogged()){
			header("Location: ".$SITE_URL);
		}
	}
	
}
function setLoginError($message){
	if(!isset($_SESSION)) { 
        session_start(); 
    } 
	$_SESSION['error'] = $message;
	/*$_SESSION['error'] = (object) [
		'message' => $message,
		'isErrorReaded'=> false
	];*/
}
function getLoginError(){
	if(!isset($_SESSION)) { 
        session_start(); 
    } 
	if(isset($_SESSION['error'])){
		echo $_SESSION['error'];
		unset($_SESSION['error']);
	} else {
		return false;
	}
}
function hasLoginError(){
    if(!isset($_SESSION)) { 
        session_start(); 
    } 
	if(isset($_SESSION['error'])){
		return true;
	} else {
		return false;
	}
}
function logoff(){
	if(!isset($_SESSION)) { 
        session_start(); 
    } 
	if(isset($_SESSION['auth'])){
		unset($_SESSION['auth']);
	}
}
function isUserAdmin(){
	if(!isset($_SESSION)) { 
        session_start(); 
    } 
    if(in_array($_SESSION['user_id'], admins())){
    	return true;
    } else {
    	false;
    }
}
function admins(){
	return array(1372, 264);
}
function setAdminArea($SITE_URL){
	if (!isUserAdmin()){
		header("Location: ".$SITE_URL);
	}	
}

function getHeader(){
	include_once '../templates/header.php';
}
function getFooter(){
	include_once '../templates/footer.php';
}

?>

