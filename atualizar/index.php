
	<?php 
		include_once '../functions.php'; 
		setLoggedArea(true, $SITE_URL);
		setAdminArea($SITE_URL);
		$title = 'Programa de Dependência';
		getHeader();
	?>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6 pl-5 text-center">
				<h3 id='page-intro' class="text-muted">Atualizar banco de dados</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8 pl-5">
				<p class="text-center text-muted" id='lead'> Clique no botão abaixo para atualizar o banco de dados de reprovações, o sistema precisará de alguns minutos. Então não feche essa página enquanto não for instruído a fazê-lo. </p>
				<form id='updateform' method="post" action="update.php">
				<div id="btn-holder">
					<button type="submit" id='submitBtn' class="btn btn-primary btn-lg btn-block">Atualizar</button>

				</div>

			</div>
		</div>
		

<?php function scripts(){ ?>
<script type="text/javascript">
	
	const btnholder = document.getElementById('btn-holder');
	const btn = document.getElementById('submitBtn');
	const updateform = document.getElementById('updateform');
	const introText = document.getElementById('page-intro');
	const leadText = document.getElementById('lead');
	

	btn.addEventListener('click', e => {
		btnholder.innerHTML = '<center><div class="spinner-grow" role="status"><span class="sr-only">Loading...</span></div></center>'
		lead.innerHTML = '';
		introText.innerHTML = 'Atualizando..'
		updateform.submit();
	})
	

</script>
<?php } ?>
<?php getFooter(); ?>
</html>

