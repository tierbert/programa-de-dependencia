<?php 
include_once '../functions.php';

if(isset($_POST['login']) && isset($_POST['senha'])){
	$login = $_POST['login'];
	$pass = $_POST['senha'];
 	
 	if(authUser($login, $pass)){
 		header("Location: ".$SITE_URL);	
 		
 	} else {
 		setLoginError('Usuário ou senha incorreto, por favor verifique os seus dados');
 		header("Location: ".$SITE_URL.'autenticar');	
 		
 	}
} else {
	setLoginError('Usuário ou senha não informados, por favor verifique se o formulário está completamente preenchido');
	header("Location: ".$SITE_URL.'autenticar');
	
}




?>