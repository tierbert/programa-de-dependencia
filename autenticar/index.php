
	<?php 
	include_once '../functions.php'; 
	$title = 'Programa de Dependência - Autenticação';
	getHeader();

	setLoggedArea(false, $SITE_URL);
	?>

		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6 pl-5 text-center">
				<h3 id='page-intro' class='text-muted'>Efetuar Login</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8 pl-5">
				<?php if (hasLoginError()){ ?>
					<p class="lead text-center alert alert-danger" id='alert'> <?php getLoginError(); ?> </p>	
				<?php } else { ?>
					<p class="lead text-center" id='alert'> Informe o seu usuário e senha do JACAD para acessar o sistema </p>
				<?php } ?>
				<form id='updateform' method="post" action="login.php">
					<div class="form-group">
				    <input type="text" class="form-control" name="login" id="login" placeholder="Insira o CPF" required>
				  </div>
				  <div class="form-group">
				    <input type="password" class="form-control" name="senha" id="senha" placeholder="Insira a Senha" required>
				  </div>
					
				<div id="btn-holder">
					<button type="submit" id='submitBtn' class="btn btn-primary btn-lg btn-block">Entrar</button>
				</div>

			</div>
		</div>
		


<?php function scripts(){ ?>
	<script type="text/javascript">
	
		const btnholder = document.getElementById('btn-holder');
		const btn = document.getElementById('submitBtn');
		const updateform = document.getElementById('updateform');
		const introText = document.getElementById('page-intro');
		const lead = document.getElementById('alert');
		const login = document.getElementById('login');
		const senha = document.getElementById('senha');
		const alert = document.getElementById('alert');

		btn.addEventListener('click', e => {
			
			if(login.value != '' && senha.value != '' && login.value != null && senha.value != null){
				console.log(login.value, senha.value);
				btnholder.innerHTML = '<center><div class="spinner-grow" role="status"><span class="sr-only">Loading...</span></div></center>'
				alert.innerText = '';
				alert.classList.remove('alert');
				alert.classList.remove('alert-danger');
				introText.innerHTML = 'Efetuando login..';
				updateform.submit();	
			} else {
				alert.classList.add('alert');
				alert.classList.add('alert-danger');
				alert.innerText = 'Certifique-se de preencher todos os dados do formulários';
			}
			
			
			e.preventDefault();
		})
		

	</script>
<?php } ?>
<?php getFooter(); ?>
