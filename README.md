**Como importar e instalar**

1. Faça a clonagem do projeto
2. Crie um banco de dados
3. Crie um arquivo chamado vars.php contendo 4 variáveis

	* *mydb*: Nome do banco de dados
	* *host*: Servidor do banco de dados
	* *user*: Usuário do BD
	* *pass*: Senha do BD

**Este projeto é um projeto privado do Centro Universitario UniFG certifique-se de obter permissão para estar aqui.**