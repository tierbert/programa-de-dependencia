

<?php
	include_once '../functions.php';
	setLoggedArea(true, $SITE_URL);
	$title = 'Programa de Dependência';
	getHeader();
	// $CONNECTION has data of the connection;
	// $data = getDataFromDB($CONNECTION);
	
?>	
	

		<div class="row">
			<div class="col-md-3 text-center">
				<a href="<?php echo $SITE_URL.'autenticar/logoff.php' ?>" class="btn btn-primary">Fazer logoff</a>	
			</div>
			<div class="col-md-6 pl-4">
				<h3 class="text-muted">Relatório de Reprovações de Alunos</h3>
			</div>
			<div class="col-md-3">
				<?php if(isUserAdmin()){ ?>
					<a href="<?php echo $SITE_URL.'atualizar' ?>" class="btn btn-primary">Atualizar Banco de Dados</a>		
				<?php } ?>
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 offset-3 pl-5">
				<p class="text-muted"> Clique nos nomes das colunas para mudar a ordenação </p>
			</div>
		</div>
		<table id='alunosDisc' class="table table-striped table-bordered dt-responsive">
		  <thead>
		    <tr>
		      <th scope="col">Matrícula</th>
		      <th scope="col">Aluno</th>
		      <th scope="col">Disciplina</th>
		      <th scope="col">Curso</th>
		      <th scope="col">Repetições</th>
		    </tr>
		  </thead>
		</table>
		<div class="text-right">
			<a href="<?php echo $SITE_URL ?>exportar" class="btn btn-primary">Exportar dados</a>
		</div>


<?php function scripts(){ ?>
<script>
	$(document).ready( function () {
	    $('#alunosDisc').DataTable({
	   		"language": {
	            "lengthMenu": "Mostrar _MENU_ registros por página",
	            "zeroRecords": "Nada encontrado, vefique os seus filtros",
	            "info": "Página _PAGE_ de _PAGES_",
	            "infoEmpty": "Nenhum registro disponível",
	            "infoFiltered": "(filtrado entre _MAX_ registros)",
	            "search": "Pesquisar:",
	            "paginate": {
			        "first":      "Primeiro",
			        "last":       "Último",
			        "next":       "Próximo",
			        "previous":   "Anterior"
			    },
			    "aria": {
			        "sortAscending":  ": ative para ordenar a table de forma crescente",
			        "sortDescending": ": ative para ordenar a table de forma decrescente"
			    },
			    
        	},
    		"bProcessing": true,
	        "serverSide": true,
	        "ajax":{
	            url :"response.php", // json datasource
	            type: "post",  // type of method  , by default would be get
	            error: function(){  // error handling code
	              $("#alunosDisc").css("display","none");
	            }
	        }
	    });
	} );
</script>
<?php } ?>
<?php getFooter(); ?>
