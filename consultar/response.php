
<?php
	//include_once CONNECTIONection file 
	include_once("../functions.php");
	 
	// initilize all variable
	$params = $columns = $totalRecords = $data = array();

	$params = $_REQUEST;

	//define index of column
	$columns = array( 
		0 => 'ra',
		1 => 'nome', 
		2 => 'disciplina',
		3 => 'curso',
		4 => 'count(disciplina)'
	);

	$where = $sqlTot = $sqlRec = "";

	// check search value exist
	if( !empty($params['search']['value']) ) {   
		$where .=" WHERE ";
		$where .=" ( nome LIKE '".$params['search']['value']."%' ";    
		$where .=" OR disciplina LIKE '".$params['search']['value']."%' ";
		$where .=" OR curso LIKE '".$params['search']['value']."%' )";
	}

	// getting total number records without any search
	$sql = "SELECT ra, nome, disciplina, curso, count(disciplina) FROM `alunos_disc` ";
	$sqlTot .= "SELECT ra, nome, disciplina, curso FROM `alunos_disc` ";
	$sqlRec .= $sql;
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}

	if ($columns[$params['order'][0]['column']] && $params['order'][0]['dir']) {
		$sqlRec .=  " GROUP BY nome, disciplina ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir'].", count(disciplina) desc LIMIT ".$params['start']." ,".$params['length'].";";
	} else {
		$sqlRec .=  " GROUP BY nome, disciplina ORDER BY count(disciplina) desc LIMIT ".$params['start']." ,".$params['length'].";";
	}
 	

	$queryTot = mysqli_query($CONNECTION, $sqlTot) or die("database error:".$sqlTot);


	$totalRecords = mysqli_num_rows($queryTot);

	$queryRecords = mysqli_query($CONNECTION, $sqlRec) or die("error to fetch employees data".$sqlRec);

	//iterate on results row and create new index array of data
	while( $row = mysqli_fetch_row($queryRecords) ) { 
		$data[] = $row;
	}	

	$json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"query" => $sqlTot,
			"data"            => $data   // total data array
			);

	echo json_encode($json_data);  // send data as json format
?>
	